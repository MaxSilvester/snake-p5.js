var iwh;
var owh;
var factor;
var screen_buff;
var snake;
var snake_len;
var snake_start_len;
var food;
var xv, yv, dir_changed;
var is_loop;
var is_cursor;
var dbg;
var fps;
var show_help;
var show_fps;
var is_touchscreen;
var orientation_vert;
var touch_start, touch_end;
var params;

function noop() { }

function to_positive(x) {
  return (x < 0 ? -x : x);
}

function repeat(str, n) {
  let out = "";
  for (let i = 0; i < n; i++) {
    out += str;
  }
  return out;
}

function ask_fps() {
  let fps_str = prompt("FPS", fps);
  if (fps_str != null && fps_str.length > 0) {
    fps = int(fps_str);
    frameRate(fps);
  }
}

function rand_pos() {
  return { x: floor(random(iwh)), y: floor(random(iwh)) };
}

function pos_in_posv(pos, posv) {
  let is_in = false;
  for (let i = 0; i < posv.len; i++) {
    if (posv[i].x == pos.x && posv[i].y == pos.y) {
      is_in = true;
      break;
    }
  }
  return is_in;
}

function rand_food_pos() {
  let pos;
  do {
    pos = rand_pos();
  } while (pos_in_posv(pos, snake));
  return pos;
}

function setup() {
  createCanvas(window.innerWidth, window.innerHeight, P2D);
  orientation_vert = width >= height;
  init();
}

function init() {
  iwh = 100;
  xv = yv = 0;
  screen_buff = createImage(iwh, iwh);
  snake_start_len = 5;
  snake_len = snake_start_len;
  snake = [];
  snake[0] = { x: int(iwh / 2), y: int(iwh / 2) };
  dir_changed = false;
  food = rand_food_pos();
  is_touchscreen = "ontouchstart" in window;
  params = {};
  (new URL(document.location.href)).searchParams.forEach((value, key) => {
    if (key in params) {
      if (typeof params[key] != typeof [])
        params[key] = [params[key]];
      params[key].push(value);
    } else
      params[key] = value;
  });
  console.log(params);
  // params = getURLParams();
  fps = ("fps" in params) ? int(params.fps) : (is_touchscreen ? 20 : 60);
  show_help = ("show_help" in params) ? (isNaN(int(params.show_help)) ? boolean(params.show_help) : boolean(int(params.show_help))) : false;
  show_fps = ("show_fps" in params) ? (isNaN(int(params.show_fps)) ? boolean(params.show_fps) : boolean(int(params.show_fps))) : true;
  is_cursor = ("show_mouse" in params) ? (isNaN(int(params.show_mouse)) ? boolean(params.show_mouse) : boolean(int(params.show_mouse))) : false;
  is_loop = ("start" in params) ? (isNaN(int(params.start)) ? boolean(params.start) : boolean(int(params.start))) : true;
  dbg = ("dbg" in params) ? (isNaN(int(params.dbg)) ? boolean(params.dbg) : boolean(int(params.dbg))) : false;
  let title = ("title" in params) ? params.title : "Snake";
  let title_elem = document.createElement("title");
  title_elem.innerText = title;
  document.head.appendChild(title_elem);
  (is_cursor ? cursor : noCursor)();
  (is_loop ? noop : noLoop)();
  frameRate(fps);
  // is_touchscreen = true;
}

function clear_screen_buff() {
  const bg = color(0, 0, 0).levels;
  screen_buff.loadPixels();
  for (let i = 0; i < iwh * iwh * 4; i += 4) {
    for (let j = 0; j < bg.length; j++) {
      screen_buff.pixels[i + j] = bg[j];
    }
  }
  screen_buff.updatePixels();
}

function draw_point(x, y, r, g, b) {
  screen_buff.loadPixels();
  screen_buff.set(x, y, color(r, g, b));
  screen_buff.updatePixels();
}

function draw_points() {
  noStroke();
  image(screen_buff, 0, 0);
}

function draw() {
  background(0, 255 / 4, 255 / 4);
  owh = width > height ? height : width;
  factor = owh / iwh;
  noSmooth();
  let fps_str = frameRate().toFixed(2);
  fps_str = repeat('0', (fps.toFixed == undefined ? fps.toString() : fps.toFixed(2)).length - fps_str.length) + fps_str;
  push();
  translate((width - owh) / 2, (height - owh) / 2);
  scale(factor);
  for (let i = (snake.length - 1 < snake_len - 1 ? snake.length - 1 : snake_len - 1); i >= 0; i--) {
    snake[i + 1] = { x: snake[i].x, y: snake[i].y };
  }
  let x, y;
  x = snake[0].x;
  y = snake[0].y;
  x += xv;
  y += yv;
  dir_changed = false;
  for (let i = 0; i < snake.length; i++) {
    if (snake[i].x == x && snake[i].y == y && (xv != 0 || yv != 0)) {
      is_loop = false;
      noLoop();
      break;
    }
  }
  if (x < 0) {
    x = iwh - 1;
  }
  if (y < 0) {
    y = iwh - 1;
  }
  if (x >= iwh) {
    x = 0;
  }
  if (y >= iwh) {
    y = 0;
  }
  if (x == food.x && y == food.y) {
    snake_len++;
    food = rand_food_pos();
  }
  snake[0].x = x;
  snake[0].y = y;
  while (snake.length > snake_len) {
    snake.pop();
  }
  clear_screen_buff();
  for (let i = 0; i < snake.length; i++) {
    if (food.x == snake[i].x || food.y == snake[i].y)
      draw_point(snake[i].x, snake[i].y, 255, 255 / 2 * 1.5, 0);
    // else if (food.x == x || food.y == y)
    //   draw_point(snake[i].x, snake[i].y, 255, 255 / 2, 0);
    else
      draw_point(snake[i].x, snake[i].y, 255, 255, 255);
  }
  draw_point(food.x, food.y, 255, 0, 0);
  draw_points();
  pop();
  smooth();
  rectMode(CORNERS);
  textAlign(LEFT, TOP);
  textSize(factor * 2);
  fill(127, 255, 0);
  stroke(0, 0, 0);
  strokeWeight(factor / 4);
  strokeJoin(ROUND);
  strokeCap(ROUND);
  if (show_fps) {
    text(fps_str + " FPS", (width - owh) / 2 + factor * 1 / 2, (height - owh) / 2 + factor * 1 / 4);
  }
  textAlign(RIGHT, TOP);
  fill(255, 255, 255);
  text((snake_len - snake_start_len).toString() + " Punkte", (width - owh) / 2 + factor * (iwh - 1 / 2), (height - owh) / 2 + factor * 1 / 4);
  if (show_help) {
    textAlign(LEFT, TOP);
    text(join([
      "W, Pfeil nach oben, nach oben wischen: Nach oben",
      "A, Pfeil nach links, nach links wischen: Nach links",
      "S, Pfeil nach unten, nach unten wischen: Nach unten",
      "D, Pfeil nach rechts, nach rechts wischen: Nach rechts",
      "H, F1: Hilfe",
      "V, F11: Vollbild",
      "P, Leertaste: Pause",
      "C: Maus anzeigen",
      "R: Zurücksetzen",
      "N: Neustart",
      "F: FPS anzeigen",
      "G: FPS ändern" // (width - owh) / 2, (height - owh) / 2
    ], '\n'), (width - owh) / 2 + factor * 1, (height - owh) / 2 + factor * 2.5, (width - owh) / 2 + factor * (iwh - 1), (height - owh) / 2 + factor * (iwh - 1));
  }
  if (dbg) {
    textAlign(LEFT, TOP);
    textSize(factor * 6);
    fill(127, 255, 0);
    stroke(0, 0, 0);
    strokeWeight(6);
    text(join([
      "iwh: " + iwh.toString(),
      "owh: " + owh.toString(),
      "factor: " + factor.toString(),
      "loop: " + is_loop.toString(),
      "cursor: " + is_cursor.toString(),
      "xv: " + xv.toString(),
      "yv: " + yv.toString(),
      "fps: " + ((typeof fps == typeof 1) ? fps.toString() : fps.toSource()),
      "touchscreen: " + is_touchscreen.toString(),
      "orientation: " + (orientation_vert ? "verti" : "hori"),
      "snake: [" + join(snake.map(function (x) { return "(" + x.x.toString() + ", " + x.y.toString() + ")" }), ', ') + "]",
      "params: " + params.toSource().slice(1, -1)
    ], '\n'), 10, 10);
  }
}

function windowResized() {
  resizeCanvas(window.innerWidth, window.innerHeight);
  orientation_vert = width >= height;
}

function set_dir(dir) {
  switch (dir) {
    case 'W':
      if ((xv != 0 || yv != 1) && !dir_changed) {
        xv = 0;
        yv = -1;
        dir_changed = true;
      }
      break;
    case 'A':
      if ((xv != 1 || yv != 0) && !dir_changed) {
        xv = -1;
        yv = 0;
        dir_changed = true;
      }
      break;
    case 'S':
      if ((xv != 0 || yv != -1) && !dir_changed) {
        xv = 0;
        yv = 1;
        dir_changed = true;
      }
      break;
    case 'D':
      if ((xv != -1 || yv != 0) && !dir_changed) {
        xv = 1;
        yv = 0;
        dir_changed = true;
      }
      break;
    default:
  }
}

function keyPressed() {
  if (dbg) {
    print('"' + key + '"', keyCode);
  }
  switch (key) {
    case 'Up':
    case 'ArrowUp':
    case 'W':
    case 'w':
    case '2':
      set_dir('W');
      break;
    case 'Left':
    case 'ArrowLeft':
    case 'A':
    case 'a':
    case '4':
      set_dir('A');
      break;
    case 'Down':
    case 'ArrowDown':
    case 'S':
    case 's':
    case '8':
      set_dir('S');
      break;
    case 'Right':
    case 'ArrowRight':
    case 'D':
    case 'd':
    case '6':
      set_dir('D');
      break;
    case 'F11':
    case 'V':
    case 'v':
      fullscreen(!fullscreen());
      break;
    case 'P':
    case 'p':
    case 'Spacebar':
    case ' ':
      (is_loop ? noLoop : loop)();
      is_loop = !is_loop;
      break;
    case 'C':
    case 'c':
      (is_cursor ? noCursor : cursor)();
      is_cursor = !is_cursor;
      break;
    case 'H':
    case 'h':
    case 'F1':
      show_help = !show_help;
      break;
    case 'G':
    case 'g':
      ask_fps();
      break;
    case 'F':
    case 'f':
      show_fps = !show_fps;
      break;
    case 'R':
    case 'r':
      let re_loop = !is_loop;
      init();
      if (re_loop) {
        loop();
      }
      is_loop = true;
      break;
    case 'N':
    case 'n':
      snake_len = snake_start_len;
      snake = [];
      snake[0] = { x: int(iwh / 2), y: int(iwh / 2) };
      food = rand_food_pos();
      xv = yv = 0;
      if (!is_loop) {
        loop();
      }
      break;
    default:
      return true;
  }
  return false;
}
/*
function mouseClicked() {
  let mx, my;
  mx = mouseX - width/2;
  my = mouseY - height/2;
  if (to_positive(mx) > to_positive(my)) {
    if (mx > 0) {
      set_dir('D');
    } else {
      set_dir('A');
    }
  } else {
    if (my > 0) {
      set_dir('S');
    } else {
      set_dir('W');
    }
  }
}
*/

function touchStarted(x) {
  if ("TouchEvent" in window && x instanceof TouchEvent) {
    touch_start = { x: x.changedTouches[0].clientX, y: x.changedTouches[0].clientY };
    return true;
  }
}
/*
function touchMoved(x) {
  if ("TouchEvent" in window && x instanceof TouchEvent) {
    return touchEnded(x);
    touch_start = {x: x.changedTouches[0].clientX, y: x.changedTouches[0].clientY};
    return true;
  }
}
*/
function touchMoved(x) {
  if ("TouchEvent" in window && x instanceof TouchEvent) {
    touch_end = { x: x.changedTouches[0].clientX, y: x.changedTouches[0].clientY };
    let difference = { x: touch_end.x - touch_start.x, y: touch_end.y - touch_start.y };
    if (to_positive(difference.x) > to_positive(difference.y)) {
      if (difference.x > 0) {
        set_dir('D');
      } else {
        set_dir('A');
      }
    } else {
      if (difference.y > 0) {
        set_dir('S');
      } else {
        set_dir('W');
      }
    }
    print(difference);
    return true;
  }
}
